function [ filt1  ] = characteristics( BW2)
%CHARACTERISTICS Summary of this function goes here
%   Detailed explanation goes here       
       
       fila_inf = round(size(BW2,1)*1/4);
       Part_inferior = BW2((fila_inf):end, 1:end);
       Im = imerode(Part_inferior,strel('disk',50));
       It = imdilate(Im,strel('disk',100));
       Tija = Part_inferior-It;
       Tija = imclearborder(Tija);

       
       Bloc_negre = zeros(round(fila_inf)-1, size(BW2,2));         
       
       te_tija = (any(any(Tija)));
       Tija = [Bloc_negre; Tija];
       
       BW2 = BW2-Tija;
       F = regionprops(BW2,'Area','Solidity','Perimeter');

       t = cell2mat(struct2cell(F));
       per = t(3)*t(3);
       Area = t(1);
%      distances = sqrt(t(3,:) - F1(1).Centroid(1)).^2 + (thisBoundary(:,2) - F1(1).Centroid(2)).^2);
       filt1 = (Area/per);
       
       F1 = regionprops(BW2,'Centroid');
       [per_row,per_col] = find(bwperim(BW2)==1);
       distances = sqrt((per_row - F1(1).Centroid(1)).^2 + (per_col - F1(1).Centroid(2)).^2);
       perimetre_minim = min(distances);
       perimetre_maxim = max(distances);
       relacio_per = perimetre_minim/perimetre_maxim;

       filt1 =  [te_tija; filt1;F.Solidity; relacio_per];


end

