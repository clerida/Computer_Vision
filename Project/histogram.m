function [av ] = histogram(I )
%HISTOGRAM Summary of this function goes here
%   Detailed explanation goes here
%     t = figure;
%     set(t,'name','fulla_9_1');
    %I = imread('l3nr012.tif');
    BW = binarize(I);
    histoR = I(:,:,1).*uint8(BW); 
    histoG = I(:,:,2).*uint8(BW); 
    histoB = I(:,:,3).*uint8(BW); 
    histoR(histoR == 0) = -1;
    avgR = mean(mean(histoR));
    histoG(histoG == 0) = -1;
    avgG = mean(mean(histoG));

    histoB(histoB == 0) = -1;
    avgB = mean(mean(histoB));
    av = [avgR;avgG;avgB];

%     subplot(2,2,1);
%     imhist(histoR(histoR >0));
%     title('Red');
%     subplot(2,2,2);
%     imhist(histoG(histoG>0));
%     title('Green');
%     subplot(2,2,3);
%     imhist(histoB(histoB>0));
%     title('Blue');
%     subplot(2,2,4);
%     imhist(rgb2gray(I));
%     title('grayscale');

end

