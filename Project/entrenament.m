close all;
clear all;
vector_classes = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15]
dirNameT = 'fulles\leaf'            %# folder path
y = ['01';'02';'03';'04';'05';'06';'07';'08';'09';'10';'11';'12';'13';'14';'15'];
yT = [];
tf = [];
meas = [];
percentil = 0.3;
mitj = [];
hist_avg = [];
hist_final = [];
for i = 1:size(vector_classes,2)
    dirName = strcat(dirNameT,int2str(vector_classes(i)),'\')
    
    imagefiles = dir( fullfile(dirName,'*.tif') )
    nfiles = length(imagefiles);    % Number of files found
    hist_avg = [];
    for ii=1:nfiles*percentil
       currentfilename = imagefiles(ii).name
       I = imread(strcat(dirName, currentfilename));
       
       hist_avg = [hist_avg histogram(I)];
       BW2 = binarize(I);
       
       tf = [tf characteristics(BW2)];
        yT = [yT; y(vector_classes(i),1:end)];

    end
    hist_final = [hist_final mean(hist_avg.')']
    mitj = [mitj  mean(tf.')']
    meas = [meas; tf'];
    tf = [];
        
end


%f = TreeBagger(100,mitj',y);

Mdl = fitcknn(meas,yT,'NumNeighbors',20,'Standardize',1);

    
   
    
    
    
    
    
    
    
    
%     %test
%     
%     I = imread('l3nr061.tif');
%     av_temporal = histogram(I);
%     sum_dif = [];
% 
%     
%     for i = 1:15
%         files_sum_dif = [];
%         for j = 1:3
%             files_sum_dif = [files_sum_dif; (av_temporal(j)-hist_final(j,i)).^2];
%         end
%         sum_dif = [sum_dif files_sum_dif];
%     end
%     
%     sum(sum_dif)
    
    
    
    
    

   
%    
% for ii = nfiles*percentil:nfiles
%     
%    currentfilename = imagefiles(ii).name;
%    I = imread(currentfilename);
%    images{ii} = I;
%    BW = rgb2gray(I);
%    BW2 = BW < 210;
%    BW2 = imfill(BW2, 'holes');
%    BW2 = bwareaopen(BW2,100000);
%    F = regionprops(BW2,'Area','Solidity','Perimeter');
%    t = cell2mat(struct2cell(F));
%    per = t(3)*t(3);
%    Area = t(1);
%    filt1 = (per/Area);
%    tf = [tf [filt1;F.Solidity]];
%     
% end
% 
% imshow(BW);




%    h = fspecial('sobel');
%    BW = edge(BW,'Sobel',0.1);
%    BW_h = imfilter(BW,h);
%    BW_v = imfilter(BW, h');
