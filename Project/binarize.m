function [ BW ] = binarize( I )
%BINARIZE Summary of this function goes here
%   Detailed explanation goes here
    BW = rgb2gray(I);
    BW = BW < 210;
    BW = imfill(BW, 'holes');
    BW = bwareaopen(BW,10000);

end

