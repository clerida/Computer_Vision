tic;
dirName = 'fulles\leaf';
resultat = zeros(16);
resultat(2:end,1) = 1:15;
resultat(1,2:end) = 1:15;
roc_plot_mat = zeros(15,size(vector_classes,2)*(70*percentil)+1);
roc_plot_mat_res = zeros(15,size(vector_classes,2)*(70*percentil)+1);
for z = 1:size(vector_classes,2)
    sum_dif = [];
    prediction = [];
    trobar = [];
    carac = [];
    result = [];
    dirNameR = strcat(dirName,int2str(vector_classes(z)),'\')
    imagefiles = dir( fullfile(dirNameR,'*.tif'));
    nfiles = length(imagefiles); 
    tic;
    for x = nfiles*percentil:nfiles
        roc_plot_mat(vector_classes(z), ((nfiles*percentil)+1)*(z-1) + (x-(nfiles*percentil)+1)) = 1;
        currentfilename = imagefiles(x).name
        I = imread(strcat(dirNameR, currentfilename));
        av_temporal = histogram(I);

        BW2 = binarize(I); %Binaritzar
        xR = characteristics(BW2);
        carac = [carac xR];
        sum_dif = [];

        ponderacions = (min(sum(sum_dif))./sum(sum_dif));
        %equalTo = find(max(ponderacions) == ponderacions);
        %prediction = [prediction [x; equalTo]];

        [y scores] = predict(Mdl, xR');
        trobar = [trobar find(max(scores) == scores)];

        max_scores = [];
        max_scores_val = [];
        scores1 = scores;
        for i = 1:3                     %trobar els 3 valors mes encertats segons el predict
            equalTo = find(max(scores) == scores,1);
            max_scores = [max_scores; equalTo];
            max_scores_val = [max_scores_val scores(equalTo)];        
            scores(equalTo) = 0;
        end
        prediction = [prediction max_scores];

        for i = 1:size(max_scores,1)
            files_sum_dif = [];
            for j = 1:3
                tmp = (av_temporal(j)-hist_final(j,max_scores(i))).^2;
                %xi^2
                tmp = tmp/abs((av_temporal(j)-hist_final(j,max_scores(i))));

                files_sum_dif = [files_sum_dif; tmp];
            end
            sum_dif = [sum_dif files_sum_dif]; 
        end
        sum_dif = sum(sum_dif);%sum_dif conte la suma de les diferencies en xi^2, quan mes petita sigui millor
        result_hist = sum_dif./max_scores_val;
        equalTo = find(min(result_hist) == result_hist); %equalTo apunta a la pos del valor minim seleccionat
        result = [result max_scores(equalTo)];  %agafem la fulla resultant 
        resultat(vector_classes(z)+1,(vector_classes(max_scores(equalTo)))+1)=resultat(vector_classes(z)+1,(vector_classes(max_scores(equalTo)))+1)+1;
        roc_plot_mat_res(max_scores(equalTo), ((nfiles*percentil)+1)*(z-1) + (x-(nfiles*percentil)+1)) = 1;

    end
    toc
    trobar

    resultat
end
resultat
percentatge = 0;
for i = 2 : 16
percentatge = percentatge + resultat(i,i);
end
percentatge = percentatge/(15*(1-percentil)*70)
toc;

% siz = prediction(2,prediction(2,:) ==15);
% size(siz,2)
% siz = prediction(2,prediction(2,:) ==11);
% size(siz,2)
% siz = prediction(2,prediction(2,:) ==9);
% size(siz,2)
% siz = prediction(2,prediction(2,:) ==3);
% size(siz,2)
% siz = prediction(2,prediction(2,:) ==1);
% size(siz,2)
